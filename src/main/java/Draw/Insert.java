package Draw;

import StateTable.Empty;
import StateTable.Field;
import StateTable.FieldState;

public class Insert implements PenState{
    private FieldState fieldState =  new Empty();

    @Override
    public void changePenState(Pen pen){
        pen.setPenState(new Insert());
    }

    @Override
    public FieldState getFieldState() {
        return fieldState;
    }


}
