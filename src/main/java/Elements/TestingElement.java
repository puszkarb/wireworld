package Elements;

import StateTable.Conductor;
import StateTable.Field;
import StateTable.FieldsTable;

public class TestingElement implements Element {

    private FieldsTable fieldsTable = new FieldsTable(1,2);
    private int height = 1;
    private int width = 2;


    public TestingElement(){
        fieldsTable.findFieldByPosition(0,0).setFieldState(new Conductor());
        fieldsTable.findFieldByPosition(1,0).setFieldState(new Conductor());
    }

    @Override
    public FieldsTable getFieldsTable() {
        return fieldsTable;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }
}
