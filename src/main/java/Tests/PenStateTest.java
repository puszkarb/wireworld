package Tests;

import Draw.DrawingHead;
import Draw.DrawingWire;
import Draw.Pen;
import StateTable.FieldsTable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PenStateTest {

    private Pen pen;

    @Test
    void changePenStateTest(){
        pen = new Pen(new FieldsTable(9,9));
        pen.setPenState(new DrawingWire());
        pen.changePenState(pen);
        if (pen.getPenState() instanceof DrawingWire)
            System.out.println("Kabel");
        if (pen.getPenState() instanceof DrawingHead)
            System.out.println("Glowa");
        else{
            System.out.println(pen.getPenState().toString());
        }
        assertTrue(pen.getPenState() instanceof DrawingHead);
        }
}
